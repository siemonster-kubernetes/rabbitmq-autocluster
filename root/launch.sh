#!/bin/bash

set -ex

# add debug
if [[ -n "$DEBUG" ]]; then
	set -x
fi

if [[ -n "$RABBITMQ_ERLANG_COOKIE" ]]; then
	cookieFile='/var/lib/rabbitmq/.erlang.cookie'
	if [ "$(cat "$cookieFile" 2>/dev/null)" != "$RABBITMQ_ERLANG_COOKIE" ]; then
		echo >&2
		echo >&2 "warning: $cookieFile contents do not match RABBITMQ_ERLANG_COOKIE"
		echo >&2
	fi
	echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
	chmod 600 "$cookieFile"
fi

chown -R rabbitmq:rabbitmq ${HOME}
exec gosu rabbitmq "$@"
